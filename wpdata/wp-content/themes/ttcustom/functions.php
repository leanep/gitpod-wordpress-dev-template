<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    $parenthandle = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
    $theme = wp_get_theme();
    wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css', 
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array( $parenthandle ),
        $theme->get('Version') // this only works if you have Version in the style header
    );
}


/*
* Creating a function to create our CPT
*/
 
function concert_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Concerts', 'Post Type General Name', 'ttcustom' ),
        'singular_name'       => _x( 'Concert', 'Post Type Singular Name', 'ttcustom' ),
        'menu_name'           => __( 'Concerts', 'ttcustom' ),
        'parent_item_colon'   => __( 'Parent Concert', 'ttcustom' ),
        'all_items'           => __( 'Tous les Concerts', 'ttcustom' ),
        'view_item'           => __( 'View Concert', 'ttcustom' ),
        'add_new_item'        => __( 'Ajouter un concert', 'ttcustom' ),
        'add_new'             => __( 'Ajouter un concert', 'ttcustom' ),
        'edit_item'           => __( 'Edit Concert', 'ttcustom' ),
        'update_item'         => __( 'Update Concert', 'ttcustom' ),
        'search_items'        => __( 'Rechercher un concert', 'ttcustom' ),
        'not_found'           => __( 'Not Found', 'ttcustom' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'ttcustom' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'concerts', 'ttcustom' ),
        'description'         => __( 'Concerts', 'ttcustom' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        //'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
     
    // Registering your Custom Post Type
    register_post_type( 'concerts', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'concert_post_type', 0 );